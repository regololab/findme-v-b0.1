# README #
FindMe beta 0.1

ricerca file o il loro contenuto all'interno di un percorso specificato.

Testato in Linux Mint 16 - 17

NON TESTATO IN WINDOWS

### FindMe v b0.1 ###

[1]- verifica l'esistenza di eventuali file all'interno di una directory

Esempio: /var/www/*.php - cerca tutti i file con estensione .php

/var/www/[0-9]*.* - cerca tutti i file che iniziano per un numero

/var/www/*Prova.* - cerca tutti i file che finiscono per Prova

/var/www/* - cerca tutto, file e directory

[2]- legge il contenuto dei file in una directory

Inserire il percorso. Esempio /var/www/

Inserire la parola da ricercare. Esempio -prova-

Se la parola viene preceduta da i\_\_ (Es. i\_\_prova) la ricerca sarà case-insensitive altrimenti verrà eseguita una ricerca per parola esatta.

l'applicazione restituirà una lista di file contenenti la parola ricercata

[3]- legge il contenuto dei file in una directory e relative sotto-directoy

Inserire il percorso. Esempio /var/www/

Inserire la parola da ricercare. Esempio -prova-

Se la parola viene preceduta da i\_\_ (Es. i\_\_prova) la ricerca sarà case-insensitive  altrimenti verrà eseguita una ricerca per parola esatta.

l'applicazione restituirà una lista di file contenenti la parola ricercata

4- [H] o [h]: visualizza le istruzioni d'uso