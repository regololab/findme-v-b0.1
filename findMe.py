#!/usr/bin/env python
#-*- coding:utf-8 -*-

import glob
import os

class FindMe():


    ## Esegue lo script di ricerca file
    def viewFind(self):
        tipo_ricerca = raw_input("Seleziona Tipo di ricerca:\n [1] File\n "\
            "[2] Testo nei file in una directory specifica\n "\
            "[3] Testo nei file in una directory e le sue sotto-direcory\n "\
            "Premi [h] per maggiori info \n>> ")
        # verifico che contenga solo numeri
        if tipo_ricerca.isdigit():
            # Se ricerco u file
            if int(tipo_ricerca) == 1:
                risultato = self.__findFile()
                n = 1
                for R in risultato:
                    # verifico se è un file o una directory
                    if os.path.isdir(R):
                        tipo = ' >> info: DIR/'
                    else:
                        # recupero l'estensione del file
                        extension = os.path.splitext(R)
                        tipo = ' >> info: FILE ' + extension[1]
                    print str(n) + ' - [ ' + R + ' ] ' + tipo
                    n = n + 1
            # Se cerco il contenuto di un file in una specifica directory
            elif int(tipo_ricerca) == 2:
                self.__findInFile()
            # Se cerco il contenuto di un file in una directory e tutte le sue sotto directory
            elif int(tipo_ricerca) == 3:
                self.__findInFileAllDir()
            else:
                print "Valore '{0}' non consentito!".format(tipo_ricerca)
        else:
            # se invece di un numero premo H (Help) oppure O (Option) visualizzo le opzioni disponibili
            if tipo_ricerca == 'h' or tipo_ricerca == 'H' or tipo_ricerca == 'o' or tipo_ricerca == 'O':
                print "+-----------------------------------------------------------------------------------+"
                print "| FindMe v b0.1:                                                                    |"
                print "| Realizzaro da Leo Lo Tito 2014                                                    |"
                print "| Rif.: leo.lotito@gmail.com                                                        |"
                print "| Potete copiare, regalare, distribuire... fare quello che volete di questo codice  |"
                print "| Ovvimente non mi assumo alcuna responsabilità per danni a cose o persone :-D      |"
                print "| Un GRAZIE di CUORE a chiunche utilizzando lo script lasci un mio riferimento      |"
                print "+-----------------------------------------------------------------------------------+\n"
                print "UTILIZZO:\n\n"\
                    "1: verifica l'esistenza di eventuali file all'interno di una directory\n"\
                    "   Esempio:\n   /var/www/*.php - cerca tutti i file con estensione .php\n"\
                    "   /var/www/[0-9]*.* - cerca tutti i file che iniziano per un numero\n"\
                    "   /var/www/*Prova.* - cerca tutti i file che finiscono per Prova\n"\
                    "   /var/www/* - cerca tutto, file e directory\n\n"\
                    "2: legge il contenuto dei file in una directory\n"\
                    "   Inserire il percorso. Esempio /var/www/\n"\
                    "   Inserire la parola da ricercare. Esempio -prova-\n"\
                    "   Se la parola viene preceduta da i__ (Es. i__prova) la ricerca sarà case insensitive\n"\
                    "   altrimenti verrà eseguita una ricerca per parola esatta\n"\
                    "   l'applicazione restituirà una lista di file contenenti la parola ricercata\n\n"\
                    "3: legge il contenuto dei file in una directory e relative sotto-directoy\n"\
                    "   Inserire il percorso. Esempio /var/www/\n"\
                    "   Inserire la parola da ricercare. Esempio -prova-\n"\
                    "   Se la parola viene preceduta da i__ (Es. i__prova) la ricerca sarà case insensitive\n"\
                    "   altrimenti verrà eseguita una ricerca per parola esatta\n"\
                    "   l'applicazione restituirà una lista di file contenenti la parola ricercata\n"
                continua = raw_input('Vuoi Continuare? S = Si; N = No:\n>>')
                if continua == 'S' or continua == 's':
                    self.viewFind()
            else:
                print "Valore '{0}' non è di tipo numerico!".format(tipo_ricerca)

    ## Esegue lo script di ricerca file
    def __findFile(self):
        descrizione = "Inserisci il percorso dei file da ricercare\nEs. /var/www/*.php\n\n>> "
        FindFile = raw_input(descrizione)
        return glob.glob(FindFile)

    def __findInFile(self):
        directory = "Inserire una directory in cui cercare\nEs. /var/www/\n\n>>"
        findFile = raw_input(directory)
        ricerca = "Inserire una chiave di ricerca\n\n>>"
        search = raw_input(ricerca)
        ## verifco se case insesitive o case sensitive
        if search.find('i__') != -1:
            caseI = 1
        else:
            caseI = 0

        n = 1
        # estraggo tutti i file presenti nella directory e sub-directory
        contenuto = os.listdir(findFile)
        if caseI == 1:
            search = search.replace('i__','')
            for R in contenuto:
                if not os.path.isdir(findFile + R):
                    f = open(findFile + R, 'r')
                    with f as testo_file:
                        testo = testo_file.read()
                        if testo.lower().find(search.lower()) != -1:
                            print str(n) + ' - [ ' + R + ' ] '
                            n = n + 1
        else:
            for R in contenuto:
                if not os.path.isdir(findFile + R):
                    f = open(findFile + R, 'r')
                    with f as testo_file:
                        testo = testo_file.read()
                        if testo.find(search) != -1:
                            print str(n) + ' - [ ' + R + ' ] '
                            n = n + 1

    def __findInFileAllDir(self):
        descrizione = "Inserire una directory in cui cercare\nEs. /var/www/\n\n>>"
        FindFile = raw_input(descrizione)
        ricerca = "Inserire una chiave di ricerca\n\n>>"
        search = raw_input(ricerca)

        filx = []
        # estraggo tutti i file presenti nella directory e sub-directory
        for root, dirs, files in os.walk(FindFile):
            level = root.replace(FindFile, '')
            directory = str(FindFile) + str(level) + '/'
            directory = directory.replace('//', '/')
            for f in files:
                filx.append(directory + f)

        ## verifco se case insesitive o case sensitive
        if search.find('i__') != -1:
            caseI = 1
        else:
            caseI = 0
        n = 1
        # ciclo e verifico il contenuto dei file
        if caseI == 1:
            search = search.replace('i__','')
            for R in filx:
                f = open(R, 'r')
                with f as testo_file:
                    testo = testo_file.read()
                    if testo.lower().find(search.lower()) != -1:
                        print str(n) + ' - [ ' + R + ' ] '
                        n = n + 1
        else:
            for R in filx:
                f = open(R, 'r')
                with f as testo_file:
                    testo = testo_file.read()
                    if testo.find(search) != -1:
                        print str(n) + ' - [ ' + R + ' ] '
                        n = n + 1


FindMe = FindMe()
FindMe.viewFind()
